module gitlab.com/ethanbakerdev/cpick

go 1.14

require (
	github.com/gdamore/tcell v1.3.0
	github.com/lucasb-eyer/go-colorful v1.0.3
	github.com/mattn/go-runewidth v0.0.9
	github.com/rivo/uniseg v0.1.0
	gitlab.com/ethanbakerdev/colors v0.0.0-20200515042549-c7fad12b1244
)
